package inno.maslakov.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by sa on 01.03.17.
 */
@Component
public class DataHandler {

    @Autowired
    private Downloader downloader;
    @Autowired
    private Uploader uploader;

    public DataHandler(Downloader downloader, Uploader uploader) {
        this.downloader = downloader;
        this.uploader = uploader;
    }

    public DataHandler(){
//        downloader = new inno.maslakov.spring.FileDownloader();
//        uploader = new inno.maslakov.spring.DbUploader();
    }
    public void hadleData(String srcPath, String destPath){
        String content = this.downloader.download(srcPath);
        String handledContent = handle(content);
        this.uploader.upload(destPath, handledContent);
    }

    private String handle(String content) {
        return null;
    }

    public void setUploader(Uploader uploader) {
        this.uploader = uploader;
    }


    public void setDownloader(Downloader downloader) {
        this.downloader = downloader;
    }
}