package inno.maslakov.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by sa on 01.03.17.
 */
@Configuration
@ComponentScan("inno.maslakov.spring")
public class AppConfig {
}
