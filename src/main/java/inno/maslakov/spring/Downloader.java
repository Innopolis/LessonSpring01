package inno.maslakov.spring;

/**
 * Created by sa on 01.03.17.
 */
public interface Downloader {
    /**
     *
     * @param path
     * @return
     */
    public String download(String path);
}