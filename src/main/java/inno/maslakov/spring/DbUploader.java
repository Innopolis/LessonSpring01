package inno.maslakov.spring;

import org.springframework.stereotype.Component;

/**
 * Created by sa on 01.03.17.
 */
@Component
public class DbUploader implements Uploader {
    public boolean upload(String path, Object content) {
        System.out.println("upload to DB");
        return true;
    }
}
