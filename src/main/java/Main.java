import inno.maslakov.spring.AppConfig;
import inno.maslakov.spring.DataHandler;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by sa on 01.03.17.
 */
public class Main {
    public static void main(String[] args) {
        AbstractApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class);
        DataHandler dataHandler = (DataHandler) context.getBean("dataHandler");
        dataHandler.hadleData("str1", "str2");
    }
}
